/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author limin
 */
package MatrixFactorization.src.main.java;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaDoubleRDD;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.mllib.recommendation.ALS;
import org.apache.spark.mllib.recommendation.MatrixFactorizationModel;
import org.apache.spark.mllib.recommendation.Rating;
import org.apache.spark.rdd.RDD;
import org.apache.spark.storage.StorageLevel;

import scala.Tuple2;

public class MFApp {

	public static void main(String[] args) {
		if (args.length < 5) {
			System.out
					.println("usage: <input> <output> <rank> <maxIterations> <lambda> <storageLevel>");
			System.exit(0);
		}
		String input = args[0];
		String output = args[1];
		int rank = Integer.parseInt(args[2]);
		int numIterations = Integer.parseInt(args[3]);
		double lambda = Double.parseDouble(args[4]);
		String sl = args[5];

		Logger.getLogger("org.apache.spark").setLevel(Level.WARN);
		Logger.getLogger("org.eclipse.jetty.server").setLevel(Level.OFF);

		SparkConf conf = new SparkConf().setAppName("MFApp Example");
		JavaSparkContext sc = new JavaSparkContext(conf);

		// Get the storage level
		StorageLevel level = StorageLevel.MEMORY_ONLY();
		// if specified use the procer storage level
		if (sl.equals("MEMORY_AND_DISK_SER"))
			level = StorageLevel.MEMORY_AND_DISK_SER();
		else if (sl.equals("MEMORY_AND_DISK"))
			level = StorageLevel.MEMORY_AND_DISK();
		else if (sl.equals("MEMORY_ONLY"))
			level = StorageLevel.MEMORY_ONLY();
		else if (sl.equals("MEMORY_ONLY_SER"))
			level = StorageLevel.MEMORY_ONLY_SER();
		else if (sl.equals("DISK_ONLY"))
			level = StorageLevel.DISK_ONLY();

		// Load and parse the data

		JavaRDD<String> data = sc.textFile(input);
		JavaRDD<Rating> ratings = data.map(new Function<String, Rating>() {
			public Rating call(String s) {
				String[] sarray = s.split(",");
				return new Rating(Integer.parseInt(sarray[0]), Integer
						.parseInt(sarray[1]), Double.parseDouble(sarray[2]));
			}
		});
		ratings.persist(level);

		RDD<Rating> parsed_data = JavaRDD.toRDD(ratings);
		parsed_data.persist(level);

		// Build the recommendation model using ALS
		MatrixFactorizationModel model = ALS.train(parsed_data, rank,
				numIterations, lambda);

		// Evaluate the model on rating data
		JavaRDD<Tuple2<Object, Object>> userProducts = ratings
				.map(new Function<Rating, Tuple2<Object, Object>>() {
					public Tuple2<Object, Object> call(Rating r) {
						return new Tuple2<Object, Object>(r.user(), r.product());
					}
				});

		userProducts.persist(level);

		JavaPairRDD<Tuple2<Integer, Integer>, Double> predictions = JavaPairRDD
				.fromJavaRDD(model
						.predict(JavaRDD.toRDD(userProducts))
						.toJavaRDD()
						.map(new Function<Rating, Tuple2<Tuple2<Integer, Integer>, Double>>() {
							public Tuple2<Tuple2<Integer, Integer>, Double> call(
									Rating r) {
								return new Tuple2<Tuple2<Integer, Integer>, Double>(
										new Tuple2<Integer, Integer>(r.user(),
												r.product()), r.rating());
							}
						}));
		JavaRDD<Tuple2<Double, Double>> ratesAndPreds = JavaPairRDD
				.fromJavaRDD(
						ratings.map(new Function<Rating, Tuple2<Tuple2<Integer, Integer>, Double>>() {
							public Tuple2<Tuple2<Integer, Integer>, Double> call(
									Rating r) {
								return new Tuple2<Tuple2<Integer, Integer>, Double>(
										new Tuple2<Integer, Integer>(r.user(),
												r.product()), r.rating());
							}
						})).join(predictions).values();

		double MSE = JavaDoubleRDD.fromRDD(
				ratesAndPreds.map(
						new Function<Tuple2<Double, Double>, Object>() {
							public Object call(Tuple2<Double, Double> pair) {
								Double err = pair._1() - pair._2();
								return err * err;
							}
						}).rdd()).mean();
		System.out.println("Mean Squared Error = " + MSE);
		sc.stop();
	}
}
