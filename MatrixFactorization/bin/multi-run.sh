#!/bin/sh
bin=`dirname "$0"`
bin=`cd "$bin"; pwd`
DIR=`cd $bin/../; pwd`
SCRIPT_DIR=/var/ftp/results/analysis_tool
ANALYSIS_DIR=/var/ftp/results/incoming
echo "========== running ${APP} benchmark =========="
file=$DIR/bin/config.sh
envFile=$DIR/conf/env.sh



mode="inputsize"
#mode="shuffle_mem"
#mode="partition"
#mode="mem"
#mode="mem_par"
#mode="sercmp"

if [ $mode = "partition" ]; then
	baseM=500
        baseN=200
        baseRank=10
	i=15
        totalM=`echo "$baseM*$i"|bc`;     totalM=`printf "%.0f\n" $totalM`;
        totalN=`echo "$baseN*$i"|bc`;     totalN=`printf "%.0f\n" $totalN`;
        totalRank=`echo "$baseRank*$i"|bc`;     totalRank=`printf "%.0f\n" $totalRank`;
        sed  -i '/m=/ c m='$totalM $envFile
        sed  -i '/n=/ c n='$totalN $envFile
        sed  -i '/rank=/ c rank='$totalRank $envFile


	 for n in {100..1000..100}; do 				
		sed -i "/NUM_OF_PARTITIONS=/ c NUM_OF_PARTITIONS=$n" $envFile; 
                $DIR/bin/gen_data.sh;sleep 10;
		 $DIR/bin/run.sh;sleep 10;
                APP_ID=$($SCRIPT_DIR/bin/getLatestApplicationID.sh);
                echo "Copying /app-logs/$APP_ID to $ANALYSIS_DIR/$APP_ID"
                hdfs dfs -copyToLocal /app-logs/$APP_ID .
                mv $APP_ID $ANALYSIS_DIR/$APP_ID
                sleep 30;

	done	
elif [ $mode = "sercmp" ]; then	
	#the combinations[5+1]: 
	#assuming the case of de-serialization, has been tested out
	#make sure the serialization is right; StorageLevel in configuration file
	
	#what to change: JavaSerializer, KryoSerializer; turn off compression
	#for each combination RDD cache size: need to get the right rdd size for the best performance numbers
	#best serialization + compression: snappy, lz4, lzf
	
	file=$DIR/bin/config.sh


	 
	#totmem=5.2; 	memf=`awk 'BEGIN{printf("%.2f\n",('$i'+1)/'$totmem'/10)}' `			 
	#sed -i '/memoryFraction=0/ c memoryFraction='$memf $file 		
	#echo "echo $i G data: NUM_OF_EXAMPLE=$total partition $numpar; memfraction $memf"
	sed  -i '/rdd_compression=/ c rdd_compression=false' $file	
	sed -i '/memoryFraction=0/ c memoryFraction=0.02' $file 		
	sed -i '/spark_ser=/ c spark_ser=JavaSerializer' $file			
	$DIR/bin/run.sh $APP;echo "==java serializer finished==";sleep 60;
	
	sed -i '/memoryFraction=0/ c memoryFraction=0.02' $file 		
	sed -i '/spark_ser=/ c spark_ser=KryoSerializer' $file										
    $DIR/bin/run.sh $APP; echo "==kryo serializer finished=="; sleep 60;
	
	if [ 1 -eq 1 ];then
		sed -i '/memoryFraction=0/ c memoryFraction=0.015' $file 		
		sed -i '/rdd_compression=/ c rdd_compression=true' $file	
		sed -i '/spark_ser=/ c spark_ser=KryoSerializer' $file			
		
		
		sed -i '/rddcodec=/ c rddcodec=lzf' $file				
		$DIR/bin/run.sh $APP;echo "==lzf  serializer finished==";sleep 60;
		
		sed -i '/rddcodec=/ c rddcodec=lz4' $file				
		$DIR/bin/run.sh $APP;echo "==lz4  serializer finished==";sleep 60;	
		
		sed -i '/rddcodec=/ c rddcodec=snappy' $file			
		$DIR/bin/run.sh $APP;echo "==snappy  serializer finished==";sleep 60;
	fi	
	
elif [ $mode = "shuffle_mem" ]; then
        file=$DIR/bin/config.sh
        envFile=$DIR/conf/env.sh
	i=5
	baseM=500
        baseN=200
        baseRank=10
        totalM=`echo "$baseM*$i"|bc`;     totalM=`printf "%.0f\n" $totalM`;
        totalN=`echo "$baseN*$i"|bc`;     totalN=`printf "%.0f\n" $totalN`;
        totalRank=`echo "$baseRank*$i"|bc`;     totalRank=`printf "%.0f\n" $totalRank`;
        sed  -i '/m=/ c m='$totalM $envFile
        sed  -i '/n=/ c n='$totalN $envFile
        sed  -i '/rank=/ c rank='$totalRank $envFile

        #repete expeirmment 10 times
#        for rep in {1..10..1}; do
         for rep in 1; do
                echo "Repetition: $rep of 1"
                #Generate the data (it is the same for all executions)
                $DIR/bin/gen_data.sh;

                for n in 0.05 0.1 0.15 0.2 0.25 0.3 0.35 0.4 0.45 0.5 0.55 0.6 0.65 0.7 0.75 0.8 0.85 0.9 0.95 1; do
                        echo "memoryfraction $n"
                        sed -i "/SPARK_SHUFFLE_MEMORYFRACTION=/ c SPARK_SHUFFLE_MEMORYFRACTION=$n" $envFile;

                        $DIR/bin/run.sh; sleep 10;
                        APP_ID=$($SCRIPT_DIR/bin/getLatestApplicationID.sh);
                        echo "Copying /app-logs/$APP_ID to $ANALYSIS_DIR/$APP_ID"
                        hdfs dfs -copyToLocal /app-logs/$APP_ID .
                        mv $APP_ID $ANALYSIS_DIR/$APP_ID
                        sleep 30;
                done
        done
elif [ $mode = "mem" ]; then
	bin/gen_data.sh; 
	vals="0.15 0.1 0.05"
	for n in $vals; do 	
		echo "memoryfraction $n"
		sed -i "/memoryFraction=/ c memoryFraction=$n" bin/config.sh; 
		bin/run.sh; 
	done
elif [ $mode = "mem_par" ]; then
	bin/gen_data.sh; 
	va="0.15 0.1 0.05"
	for n in $va; do 	
		echo "memoryfraction $n"
		sed -i "/memoryFraction=/ c memoryFraction=$n" bin/config.sh; 
		bin/run.sh; 
	done	
elif [ $mode = "inputsize" ]; then	
	#vals="0.15 0.1 0.05"
	# 70M =data of4G
	file=$DIR/bin/config.sh
        envFile=$DIR/conf/env.sh

     for i in {10..25..3}; do
                echo "run $i"
		baseM=500
		baseN=200
		baseRank=10
                totalM=`echo "$baseM*$i"|bc`;     totalM=`printf "%.0f\n" $totalM`;
		totalN=`echo "$baseN*$i"|bc`;     totalN=`printf "%.0f\n" $totalN`;
		totalRank=`echo "$baseRank*$i"|bc`;     totalRank=`printf "%.0f\n" $totalRank`;
                sed  -i '/m=/ c m='$totalM $envFile
		sed  -i '/n=/ c n='$totalN $envFile
		sed  -i '/rank=/ c rank='$totalRank $envFile

		$DIR/bin/gen_data.sh;sleep 10;
                $DIR/bin/run.sh;sleep 10;
                APP_ID=$($SCRIPT_DIR/bin/getLatestApplicationID.sh);
                echo "Copying /app-logs/$APP_ID to $ANALYSIS_DIR/$APP_ID"
                hdfs dfs -copyToLocal /app-logs/$APP_ID .
                mv $APP_ID $ANALYSIS_DIR/$APP_ID
                sleep 120;

        done
else 
	echo "error!";	
fi

exit 0;
