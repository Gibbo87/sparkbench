#!/bin/bash
echo "========== running KMeans benchmark =========="
bin=`dirname "$0"`
bin=`cd "$bin"; pwd`
DIR=`cd $bin/../; pwd`
envFile=$DIR/conf/env.sh
SCRIPT_DIR=/var/ftp/results/analysis_tool
ANALYSIS_DIR=/var/ftp/results/incoming


#mode="cores"
mode="inputsize"
#mode="partition"
#mode="store_mem"
#mode="shuffle_mem"
#mode="mem_par"
#mode="feature"

echo "using mode: $mode"

if [ $mode = "partition" ]; then
        file=$DIR/bin/config.sh
        envFile=$DIR/conf/env.sh


#	echo "Repetition: $rep of 5"
		#partitions loop


         for n in {100..1000..100}; do        
		  sed -i '/NUM_OF_PARTITIONS=/ c NUM_OF_PARTITIONS='$n $envFile
		  #Generate the data
		  $DIR/bin/gen_data.sh; sleep 10;
                  $DIR/bin/run.sh; sleep 10;
                  APP_ID=$($SCRIPT_DIR/bin/getLatestApplicationID.sh);
                  echo "Copying /app-logs/$APP_ID to $ANALYSIS_DIR/$APP_ID"
                  hdfs dfs -copyToLocal /app-logs/$APP_ID .
                  mv $APP_ID $ANALYSIS_DIR/$APP_ID
                  sleep 50;
         done

elif [ $mode = "store_mem" ]; then
 	file=$DIR/bin/config.sh
        envFile=$DIR/conf/env.sh

        #Select the dimension of the input data (70Gb now)
        base=11 #7.5 million data points =1G size of data
        i=70
	total=`echo "$base*$i*1000000"|bc`;     total=`printf "%.0f\n" $total`;
        sed  -i '/NUM_OF_EXAMPLES=/ c NUM_OF_EXAMPLES='$total $envFile

        #select the number of partitions (800 now)
        numpar=720
        sed -i '/NUM_OF_PARTITIONS=/ c NUM_OF_PARTITIONS='$numpar $envFile
	
	#repete expeirmment 10 times
	for rep in {1..10..1}; do
		echo "Repetition: $rep of 10"
	        #Generate the data (it is the same for all executions)
	        $DIR/bin/gen_data.sh;

        	for n in 0.05 0.1 0.15 0.2 0.25 0.3 0.35 0.4 0.45 0.5 0.55 0.6 0.65 0.7 0.75 0.8 0.85 0.9 0.95 1; do
                	echo "memoryfraction $n"
	                sed -i "/SPARK_STORAGE_MEMORYFRACTION=/ c SPARK_STORAGE_MEMORYFRACTION=$n" $envFile;
		
        	        $DIR/bin/run.sh; sleep 10;
	                APP_ID=$($SCRIPT_DIR/bin/getLatestApplicationID.sh);
	                echo "Copying /app-logs/$APP_ID to $ANALYSIS_DIR/$APP_ID"
	                hdfs dfs -copyToLocal /app-logs/$APP_ID .
	                mv $APP_ID $ANALYSIS_DIR/$APP_ID
	                sleep 30;
	        done
	done
elif [ $mode = "shuffle_mem" ]; then
        file=$DIR/bin/config.sh
        envFile=$DIR/conf/env.sh

        #Select the dimension of the input data (70Gb now)
        base=11 #7.5 million data points =1G size of data
        i=70
        total=`echo "$base*$i*1000000"|bc`;     total=`printf "%.0f\n" $total`;
        sed  -i '/NUM_OF_EXAMPLES=/ c NUM_OF_EXAMPLES='$total $envFile

        #select the number of partitions (800 now)
        numpar=720
        sed -i '/NUM_OF_PARTITIONS=/ c NUM_OF_PARTITIONS='$numpar $envFile

        #repete expeirmment 10 times
#        for rep in {1..10..1}; do
	 for rep in 1; do
                echo "Repetition: $rep of 1"
                #Generate the data (it is the same for all executions)
                $DIR/bin/gen_data.sh;

                for n in 0.05 0.1 0.15 0.2 0.25 0.3 0.35 0.4 0.45 0.5 0.55 0.6 0.65 0.7 0.75 0.8 0.85 0.9 0.95 1; do
                        echo "memoryfraction $n"
                        sed -i "/SPARK_SHUFFLE_MEMORYFRACTION=/ c SPARK_SHUFFLE_MEMORYFRACTION=$n" $envFile;

                        $DIR/bin/run.sh; sleep 10;
                        APP_ID=$($SCRIPT_DIR/bin/getLatestApplicationID.sh);
                        echo "Copying /app-logs/$APP_ID to $ANALYSIS_DIR/$APP_ID"
                        hdfs dfs -copyToLocal /app-logs/$APP_ID .
                        mv $APP_ID $ANALYSIS_DIR/$APP_ID
                        sleep 30;
                done
        done

elif [ $mode = "feature" ]; then	
	#vals="0.15 0.1 0.05"
	# 70M =data of4G
	n="0.5"
	sed -i "/memoryFraction=/ c memoryFraction=$n" bin/config.sh; 
	n="4000000"	
	sed -i "/NUM_OF_EXAMPLES=/ c NUM_OF_EXAMPLES=$n" bin/config.sh;
	n="720"
	sed -i "/NUM_OF_PARTITIONS=/ c NUM_OF_PARTITIONS=$n" bin/config.sh; 
	vals="4 8 12 16 20"
	for n in $vals; do 	
		echo "number of features $n"
		sed -i "/NUM_OF_FEATURES=/ c NUM_OF_FEATURES=$n" bin/config.sh;
		sed -i "/memoryFraction=/ c memoryFraction=0.1" bin/config.sh; 
		bin/gen_data.sh;
		sed -i "/memoryFraction=/ c memoryFraction=0.5" bin/config.sh; 
		bin/run.sh; 		
	done	
elif [ $mode = "mem_par" ]; then
	#pars="200 220 240 260 280 300 320 340"; 
	#for p in $pars; do
	n="600000000"
	sed -i "/NUM_OF_EXAMPLES=/ c NUM_OF_EXAMPLES=$n" bin/config.sh;
	for p in {800..2100..80}; do	
		sed -i "/NUM_OF_PARTITIONS=/ c NUM_OF_PARTITIONS=$p" bin/config.sh; 		
		va="0.5 0.4 0.2";
		for n in $va; do 	
			echo "memoryfraction $n"
			sed -i "/memoryFraction=/ c memoryFraction=$n" bin/config.sh; 
			bin/gen_data.sh; 
			bin/run.sh; 
		done	
	done	
elif [ $mode = "inputsize" ]; then		
	baseRows=100 
	baseColumns=5
	for n in {1..10..1}; do 	
		newRows=`echo "$baseRows*$n"|bc`;     total=`printf "%.0f\n" $total`;
		#newColumns=`echo "$baseColumns*$n"|bc`;     total=`printf "%.0f\n" $total`;
		sed -i "/ROWS=/ c ROWS=$newRows" $envFile;
		# sed -i "/COLUMNS=/ c COLUMNS=$newColumns" $envFile;
		$DIR/bin/gen_data.sh;sleep 10;
                $DIR/bin/run.sh;sleep 10;
	done
elif [ $mode = "cores" ]; then		
	n="4";
	sed -i "/NUM_OF_FEATURES=/ c NUM_OF_FEATURES=$n" $envFile;
	n="0.5"
	sed -i "/SPARK_STORAGE_MEMORYFRACTION=/ c SPARK_STORAGE_MEMORYFRACTION=$n" $envFile; 	
	n="720"
	sed -i "/NUM_OF_PARTITIONS=/ c NUM_OF_PARTITIONS=$n" $envFile; 
	vals="1 2 3 4 5 6 7 8"
	for n in $vals; do 			
		echo "number of cores $n"
		sed -i "/ecore=/ c ecore=$n" bin/config.sh;			
		bin/run.sh; 		
	done
else 
	echo "error!";	
fi

exit 0;
