package com.ibm.sparkbench.qr

import org.apache.spark.mllib.linalg.{ Matrices, Vectors, Vector }
import org.apache.spark.mllib.linalg.distributed.RowMatrix
import org.apache.spark.mllib.random.RandomRDDs
import org.apache.spark.SparkContext
import org.apache.spark.SparkConf

/**
 * @author GiovanniPaolo
 */
object QRDecompositionAppScala {

  def main(args: Array[String]) {

    val conf = new SparkConf().setAppName("QR Decomposition App Scala");
    val sc = new SparkContext(conf)

    if (args.length < 2) {
      System.out.println("usage: <input> <output>")
      System.exit(0)
    }
    val input = args(0)
    val output = args(1)
    
    val data = sc.textFile(input)
    

    var mat: RowMatrix = new RowMatrix(data.map { x => Vectors.parse(x) })
      
    val result = mat.tallSkinnyQR(true)
   
  }

}