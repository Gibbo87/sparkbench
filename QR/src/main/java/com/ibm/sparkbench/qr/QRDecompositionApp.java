package com.ibm.sparkbench.qr;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.mllib.linalg.Matrix;
import org.apache.spark.mllib.linalg.QRDecomposition;
import org.apache.spark.mllib.linalg.Vector;
import org.apache.spark.mllib.linalg.Vectors;
import org.apache.spark.mllib.linalg.distributed.RowMatrix;

public class QRDecompositionApp {

	public static void main(String[] args) {
		if (args.length < 2) {
			System.out.println("usage: <input> <output>");
			System.exit(0);
		}
		String input = args[0];
		String output = args[1];

		SparkConf conf = new SparkConf().setAppName("QR Decomposition Example");
		JavaSparkContext sc = new JavaSparkContext(conf);

		JavaRDD<String> data = sc.textFile(input);
		JavaRDD<Vector> rows = data.map(new Function<String, Vector>() {
			public Vector call(String s) {
				return Vectors.parse(s);
			}
		});
		
		
		JavaPairRDD<Vector, Vector> cartesian = rows.cartesian(rows);
		System.out.println("Cartesinan Cardinality: "+cartesian.count());
		
	}

}
