package com.ibm.sparkbench.qr;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.mllib.linalg.Vector;
import org.apache.spark.mllib.linalg.distributed.RowMatrix;
import org.apache.spark.mllib.random.RandomRDDs;

public class QRDataGen {

	public static void main(String[] args) {
		if (args.length < 4) {
			System.out.println("usage: <output> <m> <n> <partitions>");
			System.exit(0);
		}
		String output = args[0];
        int m = Integer.parseInt(args[1]);
        int n = Integer.parseInt(args[2]);
        int partitions = Integer.parseInt(args[3]);

		SparkConf conf = new SparkConf().setAppName("QR Decomposition Data Gen");
		JavaSparkContext sc = new JavaSparkContext(conf);

		JavaRDD<Vector> rows = RandomRDDs.normalJavaVectorRDD(sc, m, n, partitions);	
		
		RowMatrix matrix = new RowMatrix(rows.rdd());

		matrix.rows().saveAsTextFile(output);
	}

}
