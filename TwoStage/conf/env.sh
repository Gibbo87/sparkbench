# for prepare
SIZE=17000
FACTOR_LIN=10000000
FACTOR_QUAD=100
NUM_OF_PARTITIONS=10

SPARK_STORAGE_MEMORYFRACTION=0.6
SPARK_SHUFFLE_MEMORYFRACTION=0.2
SPARK_EXECUTOR_MEMORY=2g
SPARK_DEFAULT_PARALLELISM=$NUM_OF_PARTITIONS
