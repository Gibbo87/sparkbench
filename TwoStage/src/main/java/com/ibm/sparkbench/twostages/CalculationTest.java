package com.ibm.sparkbench.twostages;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.time.StopWatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import scala.util.Random;

public class CalculationTest {

	static final Logger logger = LoggerFactory.getLogger(CalculationTest.class);
	private static final double MIN_FACTOR = 100000000d;
	private static final double MAX_FACTOR = 1000000000d;

	public static void main(String[] args) {

		testRandom();

	}

	private static void testRandom() {
		Map<Double, Long> times = new HashMap<Double, Long>();
		Random rand = new Random();
		for (int i = 0; i < 1000; i++) {
			double num = rand.nextDouble();
			long time = calculate(num, MIN_FACTOR);
			times.put(num, time);
			logger.info("num: "+num+" time: "+time);
		}

	}

	private static void testFactor() {
		double num = new Random().nextDouble();
		logger.info("num: " + num);
		logger.info("Factor , Time");

		for (double factor = MIN_FACTOR; factor <= MAX_FACTOR; factor += (MAX_FACTOR) / 10) {
			long time = calculate(num, factor);
			logger.info(factor + "," + time);
		}
	}

	private static long calculate(double item, double factor) {
		StopWatch timer = new StopWatch();
		timer.start();
		for (int i = 0; i < factor; i++)
			Math.tan(item);
		timer.split();
		long time = timer.getSplitTime();
		timer.stop();
		return time;
	}

}
