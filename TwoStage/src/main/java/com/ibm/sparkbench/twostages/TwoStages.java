package com.ibm.sparkbench.twostages;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import scala.Tuple2;

public class TwoStages {
	static final Logger logger = LoggerFactory.getLogger(TwoStages.class);

	public static void main(String[] args) {
		if (args.length < 3) {
			System.out.println("usage: <input1> <input2> <linearFactor> <quadraticFactor>");
			System.exit(0);
		}
		String input1 = args[0];
		String input2 = args[1];		
		String factorSLin = args[2];
		String factorSQuad = args[3];

		final double linearFactor = Double.parseDouble(factorSLin);
		final double quadraticFactor = Double.parseDouble(factorSQuad);

		SparkConf conf = new SparkConf().setAppName("Two Stages");
		JavaSparkContext sc = new JavaSparkContext(conf);

		// load the first RDD
		JavaRDD<String> data1 = sc.textFile(input1);
		JavaRDD<Double> doubleVect1 = data1.map(new Function<String, Double>() {
			@Override
			public Double call(String arg0) throws Exception {
				return Double.parseDouble(arg0);
			}
		});
		
		doubleVect1.cache();
		
		logger.info("RDD1: "+doubleVect1.count());

		// load the second RDD
		JavaRDD<String> data2 = sc.textFile(input2);
		JavaRDD<Double> doubleVect2 = data2.map(new Function<String, Double>() {
			@Override
			public Double call(String arg0) throws Exception {
				return Double.parseDouble(arg0);
			}
		});
		
		doubleVect2.cache();
		
		long numbers = doubleVect2.count();
		
		logger.info("RDD2: "+numbers);
		
		// do the linear computation on the union of the two rdds
		JavaRDD<Double> unitedData = doubleVect1.union(doubleVect2);
		
		logger.info("Union : "+unitedData.count());
		unitedData = unitedData.map(new Function<Double, Double>() {
			@Override
			public Double call(Double arg0) throws Exception {
				for (int i = 0; i < linearFactor; i++)
					Math.tan(arg0);
				return Math.tan(arg0);
			}
		});
		logger.info("Liner Done");

		// do the cartesian product
		JavaPairRDD<Double, Double> cartesian = doubleVect1.cartesian(doubleVect2);
		JavaRDD<Double> cartesianMean = cartesian.map(new Function<Tuple2<Double,Double>, Double>() {
			public Double call(Tuple2<Double,Double> arg0) throws Exception {
				return (arg0._1()+arg0._2())/2;
			};
		});
		
		cartesianMean = cartesianMean.map(new Function<Double, Double>() {
			@Override
			public Double call(Double arg0) throws Exception {
				for (int i = 0; i < quadraticFactor; i++)
					Math.tan(arg0);
				return Math.tan(arg0);
			}
		});
		logger.info("Cartesian Done");
		JavaRDD<Double> subsetCartesian = cartesianMean.sample(false, 1/(double)numbers);
		
		//perform some dummy operation including both RDDs
		//long count = cartesianSum.intersection(unitedData).count();
		long count = unitedData.subtract(subsetCartesian).count();
		logger.info("Count: "+count);

	}

}
