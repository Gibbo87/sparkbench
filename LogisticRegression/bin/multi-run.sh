#!/bin/sh
echo "========== running Logistic benchmark =========="
bin=`dirname "$0"`
bin=`cd "$bin"; pwd`
DIR=`cd $bin/../; pwd`
SCRIPT_DIR=/var/ftp/results/analysis_tool
ANALYSIS_DIR=/var/ftp/results/incoming

mode="inputsize"
#mode="iter"
#mode="partition"
#mode="storage_mem"
#mode="shuffle_mem"
#mode="mem_par"
#mode="feature"
#mode="sercmp"

if [ $mode = "partition" ]; then
	n="0.5"
	sed -i "/memoryFraction=0/ c memoryFraction=$n" bin/config.sh; 
	n="70000000"
	sed -i "/NUM_OF_EXAMPLES=/ c NUM_OF_EXAMPLES=$n" bin/config.sh;
	#vals="360 480 540  720   960  180  420 600 840 1080"; 				
	vals="200 220 240 260 280 300 320 340"; 				
	#vals="60 "; 				
	for n in $vals; do 					
		sed -i "/NUM_OF_PARTITIONS=/ c NUM_OF_PARTITIONS=$n" bin/config.sh; 
		bin/gen_data.sh; 
		bin/run.sh; 
	done	
elif [ $mode = "storage_mem" ]; then

        file=$DIR/bin/config.sh
        envFile=$DIR/conf/env.sh

	#Select the dimension of the input data (50Gb now)
	base=7.5 #7.5 million data points =1G size of data
	i=15
        total=`echo "$base*$i*1000000"|bc`;     total=`printf "%.0f\n" $total`;
        sed  -i '/NUM_OF_EXAMPLES=/ c NUM_OF_EXAMPLES='$total $envFile

	#select the number of partitions (800 now)
	numpar=800
        sed -i '/NUM_OF_PARTITIONS=/ c NUM_OF_PARTITIONS='$numpar $envFile

	#Generate the data (it is the same for all executions)
	$DIR/bin/gen_data.sh; 

	for n in 0.5 0.1 0.15 0.2 0.25 0.3 0.35 0.4 0.45 0.5 0.55 0.6 0.65 0.7 0.75 0.8 0.85 0.9 0.95 1; do 	
		echo "memoryfraction $n"		
                sed -i "/SPARK_STORAGE_MEMORYFRACTION=/ c SPARK_STORAGE_MEMORYFRACTION=$n" $envFile;

               	$DIR/bin/run.sh; sleep 10;
                APP_ID=$($SCRIPT_DIR/bin/getLatestApplicationID.sh);
                echo "Copying /app-logs/$APP_ID to $ANALYSIS_DIR/$APP_ID"
                hdfs dfs -copyToLocal /app-logs/$APP_ID .
                mv $APP_ID $ANALYSIS_DIR/$APP_ID
                sleep 30;
	done
elif [ $mode = "shuffle_mem" ]; then
	echo "Benchmarking Shuffle memory fraction"
        file=$DIR/bin/config.sh
        envFile=$DIR/conf/env.sh

        #Select the dimension of the input data (70Gb now)
        base=7.5 #7.5 million data points =1G size of data
        i=10
        total=`echo "$base*$i*1000000"|bc`;     total=`printf "%.0f\n" $total`;
        sed  -i '/NUM_OF_EXAMPLES=/ c NUM_OF_EXAMPLES='$total $envFile

        #select the number of partitions (800 now)
        numpar=800
        sed -i '/NUM_OF_PARTITIONS=/ c NUM_OF_PARTITIONS='$numpar $envFile

        #repeate expeirmment 10 times
#        for rep in {1..10..1}; do
         for rep in 1; do
                echo "Repetition: $rep of 1"
                #Generate the data (it is the same for all executions)
                $DIR/bin/gen_data.sh;

                for n in 0.05 0.1 0.15 0.2 0.25 0.3 0.35 0.4 0.45 0.5 0.55 0.6 0.65 0.7 0.75 0.8 0.85 0.9 0.95 1; do
                        echo "memoryfraction $n"
                        sed -i "/SPARK_SHUFFLE_MEMORYFRACTION=/ c SPARK_SHUFFLE_MEMORYFRACTION=$n" $envFile;

                        $DIR/bin/run.sh; sleep 10;
                        APP_ID=$($SCRIPT_DIR/bin/getLatestApplicationID.sh);
                        echo "Copying /app-logs/$APP_ID to $ANALYSIS_DIR/$APP_ID"
                        hdfs dfs -copyToLocal /app-logs/$APP_ID .
                        mv $APP_ID $ANALYSIS_DIR/$APP_ID
                        sleep 30;
                done
        done
elif [ $mode = "feature" ]; then	
	#vals="0.15 0.1 0.05"
	# 70M =data of4G
	n="0.5"
	sed -i "/memoryFraction=0/ c memoryFraction=$n" bin/config.sh; 
	n="25000000"	
	sed -i "/NUM_OF_EXAMPLES=/ c NUM_OF_EXAMPLES=$n" bin/config.sh;
	n="720"
	sed -i "/NUM_OF_PARTITIONS=/ c NUM_OF_PARTITIONS=$n" bin/config.sh; 
	vals="4 8 12 16 20"
	for n in $vals; do 	
		echo "number of features $n"
		sed -i "/NUM_OF_FEATURES=/ c NUM_OF_FEATURES=$n" bin/config.sh;
		sed -i "/memoryFraction=0/ c memoryFraction=0.1" bin/config.sh; 
		bin/gen_data.sh;
		sed -i "/memoryFraction=0/ c memoryFraction=0.5" bin/config.sh; 
		bin/run.sh; 		
	done		
elif [ $mode = "mem_par" ]; then
	n="210000000"
	sed -i "/NUM_OF_EXAMPLES=/ c NUM_OF_EXAMPLES=$n" bin/config.sh;
	for((p=220;p<=2100;p+=80)); do
		sed -i "/NUM_OF_PARTITIONS=/ c NUM_OF_PARTITIONS=$p" bin/config.sh; 		
		va="0.5 0.4 0.2";
		for n in $va; do 	
			echo "memoryfraction $n"
			sed -i "/memoryFraction=0/ c memoryFraction=$n" bin/config.sh; 
			bin/gen_data.sh; 
			bin/run.sh; 
		done	
	done	
elif [ $mode = "iter" ]; then	
	vals="4 6 8 10 12"
	for n in $vals; do 	
		echo "number of iteration $n"
		sed -i "/MAX_ITERATION=/ c MAX_ITERATION=$n" bin/config.sh; 
		bin/run.sh; 
	done	
elif [ $mode = "inputsize" ]; then	
	file=$DIR/bin/config.sh
	envFile=$DIR/conf/env.sh

	for i in {2..100..4}; do
		echo "run $i"
		base=7.5 #7.5 million data points =1G size of data
		total=`echo "$base*$i*1000000"|bc`; 	total=`printf "%.0f\n" $total`;
		sed  -i '/NUM_OF_EXAMPLES=/ c NUM_OF_EXAMPLES='$total $envFile
		 
		$DIR/bin/gen_data.sh;sleep 10;
		$DIR/bin/run.sh; sleep 10;	
		APP_ID=$($SCRIPT_DIR/bin/getLatestApplicationID.sh);
                echo "Copying /app-logs/$APP_ID to $ANALYSIS_DIR/$APP_ID"
                hdfs dfs -copyToLocal /app-logs/$APP_ID .
                mv $APP_ID $ANALYSIS_DIR/$APP_ID
                sleep 30;		

        done


elif [ $mode = "sercmp" ]; then	
	#the combinations[5+1]: 
	#assuming the case of de-serialization, has been tested out
	#make sure the serialization is right; StorageLevel in configuration file
	
	#what to change: JavaSerializer, KryoSerializer; turn off compression
	#for each combination RDD cache size: need to get the right rdd size for the best performance numbers
	#best serialization + compression: snappy, lz4, lzf
	
	file=$DIR/bin/config.sh

	sed  -i '/rdd_compression=/ c rdd_compression=false' $file	
	sed -i '/memoryFraction=0/ c memoryFraction=0.346' $file 		
	sed -i '/spark_ser=/ c spark_ser=JavaSerializer' $file			
	#$DIR/bin/run.sh;echo "==java serializer finished==";sleep 60;
	
	sed -i '/memoryFraction=0/ c memoryFraction=0.79' $file 		
	sed -i '/spark_ser=/ c spark_ser=KryoSerializer' $file										
    #$DIR/bin/run.sh; echo "==kryo serializer finished=="; sleep 60;
	 
	#totmem=5.2; 	memf=`awk 'BEGIN{printf("%.2f\n",('$i'+1)/'$totmem'/10)}' `			 
	#sed -i '/memoryFraction=0/ c memoryFraction='$memf $file 		
	#echo "echo $i G data: NUM_OF_EXAMPLE=$total partition $numpar; memfraction $memf"
	
	
	
	sed  -i '/rdd_compression=/ c rdd_compression=true' $file	
	sed -i '/spark_ser=/ c spark_ser=JavaSerializer' $file			
	sed -i '/rddcodec=/ c rddcodec=snappy' $file			
	$DIR/bin/run.sh;echo "==snappy java serializer finished==";sleep 60;
	sed -i '/rddcodec=/ c rddcodec=lzf' $file				
	$DIR/bin/run.sh;echo "==lzf java serializer finished==";sleep 60;
	sed -i '/rddcodec=/ c rddcodec=lz4' $file				
	$DIR/bin/run.sh;echo "==lz4 java serializer finished==";sleep 60;
	
else 
	echo "error!";	
fi

exit 0;
