#!/bin/bash
#this script is ment to be executed aftermultiple runs of an application to aggregate the statistics generated into a single folder. 

bin=`dirname "$0"`
DIR=`cd "$bin"; pwd`

#Folder in which to look for the run_# subfolder containing the statistics of a single run
LOCAL_OUTPUT_DIR=$1

#Folder in which write the aggregated statistics
STATISTICS_DIR=$2
echo "Aggregating runs in folder: $LOCAL_OUTPUT_DIR into $STATISTICS_DIR"

mkdir $STATISTICS_DIR
mkdir $STATISTICS_DIR/stages
mkdir $STATISTICS_DIR/estimations
echo "run,size,estimatedTime,actualTime" > $STATISTICS_DIR/statistics.csv;
for run in $LOCAL_OUTPUT_DIR/run_*;
do
 run_number=$(cat $run/log.txt | egrep 'Running with i:' | cut -d ' ' -f 4);
 echo "Gathering run $run";
 cp $run/Estimation.csv $STATISTICS_DIR/estimations/$run_number.csv;
 cp $run/StageDetails.csv $STATISTICS_DIR/stages/$run_number.csv;
 size=$(cat $run/log.txt | grep Size | cut -d ' ' -f 3 | cut -d 'G' -f 1);
 estimated_time=$(cat $run/log.txt | egrep 'Total Estimated Application execution time' | cut -d ' ' -f 12);
 actual_time=$($DIR/getDuration.sh $run/application.csv);
 echo "$run_number,$size,$estimated_time,$actual_time" | tee -a $STATISTICS_DIR/statistics.csv;
done

